(function()
{

	function Slideshow(element)
	{
		this.el = document.querySelector(element);
		this.init();
		
		var self = this;
		self.timerSize = setInterval(function()
        {
			self.setCorrectWidth();
		}, 3000);
	}

	Slideshow.prototype =
	{
		init: function()
        {
			this.wrapper = this.el.querySelector(".slider-wrapper");
			this.slides = this.el.querySelectorAll(".slide");
			this.nav = this.el.querySelector(".slider-nav");
			this.previous = this.el.querySelector(".slider-previous");
			this.next = this.el.querySelector(".slider-next");
			this.index = 0;
			this.total = this.slides.length;
			this.timer = 0;
			this.moveLeft = 0;
			this.oldWidth = 0;
			this.sliderOffset = [];
			this.setCorrectWidth();
			this.stopStart();
			this.actions();
			this.timerSize = 0;
		},

		setCorrectWidth : function()
		{
			var self = this;
			var winWidth = window.innerWidth || document.documentElement.clientWidth
							 || document.body.clientWidth;
			
			if (self.oldWidth == winWidth)
			{
				return;
			}
			self.oldWidth = winWidth;
			var slideWidth = winWidth / 3;
			/* two whole slide, and two halfs */ 
			for(var i = 0; i < self.slides.length; ++i)
			{
				var slide = self.slides[i];
				slide.style.width = slideWidth + "px";
			}

			self.el.style.width = winWidth + "px";
			self.el.style.height = slideWidth * 3 / 5 + "px";
			self.nav.style.top = "-" + slideWidth * 3 / 5 + "px";
			self.wrapper.style.width = slideWidth * self.total  + "px";
			self.wrapper.style.height = "100%";		

			var diff = (slideWidth * (self.total - 3)) / self.total;	
			self.sliderOffset = [];
			var offsetLen = 0;
			var lengthSlider = slideWidth * self.slides.length;
			self.total = 0
			while (offsetLen + winWidth <= lengthSlider)
			{
				var offset = "-" + offsetLen + "px";
				self.sliderOffset.push(offset);
				
				offsetLen += diff;
				self.total++;
			}
		},

		slideNext : function()
		{
			var self = this;

			if (self.moveLeft)
			{
				self.index++;
				if (self.index > self.total)
				{
					self.index = self.total - 1;
					self.moveLeft = false;
				}
			}
			else
			{
				self.index--;
				if (self.index < 0)
				{
					self.index = 0;
					self.moveLeft = true;
				}
			}

			self.wrapper.style.left = self.sliderOffset[self.index];
		},

		slideTo: function(slide)
        {
			var currentSlide = this.slides[slide];
			this.wrapper.style.left = this.sliderOffset[this.index];
		},


        autoSlide : function()
        {
			var self = this;
		
			self.timer = setInterval(function()
            {
                self.slideNext();
            }, 2000);
        },

		setUpLinks : function()
		{
			for (var i = 0; i < this.links.length; i++)
			{
				var photoLink = self.links[i];
				photoLink.setAttribute("data-index", i);
			}
		},
             
		actions: function()
		{
			var self = this;
			
			self.next.addEventListener("click", function() 
			{
				self.index++;
				self.previous.style.display = "block";
				if (self.index >= self.total)
				{
					self.next.style.display = "none";
					self.index = self.total - 1;
				}
				else
				{
					self.next.style.display = "block";
				}
				self.slideTo(self.index);
				self.moveLeft = true;
				clearInterval(self.timer);
				self.timer = null;
			}, false);
			
			self.previous.addEventListener("click", function() 
			{
				self.index--;
 				self.next.style.display = "block";
				if (self.index <= 0)
				{
					self.previous.style.display = "none";
					self.index = 0;
				}
				else
				{
					self.previous.style.display = "block";
				}
				self.slideTo(self.index);
				self.moveLeft = false;
				clearInterval(self.timer);
                self.timer = null;
			}, false);
			
		},
		
		stopStart: function()
		{
			var self = this;
			self.el.addEventListener("mouseover", function()
			{
				clearInterval(self.timer);
				self.timer = null;
				self.previous.style.display = "block";
				self.next.style.display = "block";
				if (self.index == 0)
				{
					self.previous.style.display = "none";
				}
				else if (self.index == self.total - 1)
				{
					self.next.style.display = "none";	
				}
			}, false);

			self.el.addEventListener("mouseout", function() 
			{
				self.autoSlide();
				
			}, false);
		}

	};

	document.addEventListener("DOMContentLoaded", function() 
	{
		var slider = new Slideshow("#main-slider");
		slider.autoSlide();
	});
})();
