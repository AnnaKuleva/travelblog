(function ()
{
	function validateEmail(email) 
	{
		var re = /\S+@\S+\.\S+/;
		return re.test(email);
	}

	document.addEventListener("DOMContentLoaded", function()
	{
		var join_list_button = document.getElementById("join_my_maillist_button");
		join_list_button.addEventListener("click", function () 
		{
			var email_field = document.getElementById("added_email");	
			var input_email = email_field.value;
			if (!input_email)
			{
				alert("Вы забыли ввести e-mail");
			}
			else
			{
				if (validateEmail(input_email))
				{
					alert("К сожалению, функциональность находится в разработке. Попробуйте позже.");
				}
				else
				{
					alert("Введите корректный e-mail");
				}
			}
		})
	})
}());
