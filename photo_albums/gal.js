(function()
{
	function Gallery()
	{
		this.init();
	}

	Gallery.prototype = {
		init : function()
		{
			this.max_photos_for_page = 36;
			this.current_image = new Image();
			this.block_with_horizontal = document.getElementById("horizontal_image"); 
			this.main_horizontal_pic = document.getElementById("horizontal_gallery_image");
			this.block_with_vertical = document.getElementById("vertical_image");
			this.main_vertical_pic = document.getElementById("vertical_gallery_image");
			this.main_pic = this.main_horizontal_pic;
			this.previous = document.getElementById("previous_image_main");
			this.previous_left = document.getElementById("previous_image_left");
			this.next = document.getElementById("next_image");
			this.next_right = document.getElementById("next_image_right");
			this.read_link = document.getElementById("read_link");
			this.back_to_album_link = document.getElementById("back_to_album_link");
			this.buttonElements = document.getElementsByClassName("gallery_button");
			this.current = -1;
			this.length = -1;
			this.name = "";
			this.post_name = "";
			this.actions();	
			this.moved = false;
			this.popstate = false;
			this.windowChangedTimerSize = 0;
		},

		start_presenting : function(href_input)
		{
			var parm_values = href_input.split("?")[1];
			var params = parm_values.split("&");
			var read_link_is_enabled = true;
			if (params.length < 3)
			{
				return;
			}
			for (var i = 0; i < params.length; i++)
			{
				splited = params[i].split("=");
				if (splited[0] == "current_pic") 
				{
					this.current = parseInt(splited[1]);
				}
				else if (splited[0] == "number")
				{
					this.length = parseInt(splited[1]);	
				}
				else if (splited[0] == "main_name")
				{
					this.name = splited[1];
				}
				else if (splited[0] == "post_name")
				{
					this.post_name = splited[1];
				}
			}

			if (this.post_name == "")
				this.post_name = this.name;

			this.setup_photo();
			this.setup_back_parameter();
			if (params.length >= 4 && params[3].split('=')[0] == "show_read_link")
			{
				read_link_is_enabled = params[3].split('=')[1] == "true" ? true : false;
			}
			this.setup_read_parameter(read_link_is_enabled);
		},

		setup_back_parameter : function()
		{
			var back_url;
			if (this.length > this.max_photos_for_page)
			{
				var page_number = parseInt(this.current / this.max_photos_for_page + 1);
				back_url = this.name + "_" + page_number.toString() + ".html";
			}
			else
			{
				back_url = this.name + ".html";
			}
			this.back_to_album_link.href = back_url;
		},

		setup_read_parameter : function(is_enabled)
		{
			if (is_enabled)
			{
				var read_url = "../posts/" + this.post_name + ".html";
				this.read_link.href = read_url;
			}
			else
			{
				this.read_link.style.display = "none";
			}
		},

		setup_photo : function()
		{
			if (this.current >= this.length || this.current < 0)
			{
				this.main_pic.alt = "Ошибка! Нет такой фотографии =(";
				this.main_pic.src = "";
				return;
			}

			var str_name = "../photos/" + this.name + "/" + this.name + "_" + this.current + ".jpg";
			this.current_image.src = str_name;
			this.current_image.alt = "";

			this.setup_back_parameter();

			this.popstate = false;
		},

		setup_loaded_photo : function()
		{
			if (parseInt(this.current_image.naturalHeight) > parseInt(this.current_image.naturalWidth))
			{
				this.main_vertical_pic.src = this.current_image.src;
				this.main_vertical_pic.alt = "";
				this.main_pic = this.main_vertical_pic;
				this.block_with_horizontal.style.display = "none";
				this.block_with_vertical.style.display = "inline-block";
			}
			else
			{
				this.main_horizontal_pic.src = this.current_image.src;
				this.main_horizontal_pic.alt = "";
				this.main_pic = this.main_horizontal_pic;
				this.block_with_horizontal.style.display = "inline-block";
				this.block_with_vertical.style.display = "none";
			}

			if (this.moved && !this.popstate)
			{
				this.process_url_data(false);
			}

			self = this;
			self.windowChangedTimerSize = setTimeout(function()
			{
				self.set_correct_button_height();
			}, 0);
		},

		set_correct_button_height: function()
		{
			if (parseInt(this.current_image.naturalHeight) > parseInt(this.current_image.naturalWidth))
			{
				for (var index = 0; index < this.buttonElements.length; index++)
				{
					this.buttonElements[index].style.height = this.main_vertical_pic.height + "px";
				}
			}
			else
			{
				for (var index = 0; index < this.buttonElements.length; index++)
				{
					this.buttonElements[index].style.height = this.main_horizontal_pic.height + "px";
				}
			}
		},

		process_url_data: function(preprocessing)
		{
			var url = "show_photos.html?current_pic=" + this.current + "&number=" + this.length + "&main_name="+ this.name;
			if (this.post_name != this.name) 
			{
				url = url + "&post_name=" + this.post_name;
			}
			if (!this.moved && preprocessing)
			{
				this.moved = true;
				window.history.replaceState({"src":this.main_pic.src},"", url);
			}
			else if (!preprocessing)
			{
				window.history.pushState({"src":this.main_pic.src},"", url);
			}
		},

		select_previous_photo: function()
		{
			this.process_url_data(true);
			this.current--;
			if (this.current < 0)
			{
				this.current = this.length - 1;
			}
			this.setup_photo();
		},	

		select_next_photo: function()
		{
			this.process_url_data(true);
			this.current++;
			if (this.current >= this.length)
			{
				this.current = 0;
			}
			this.setup_photo();
		},

		actions: function()
		{
			var self = this;

			self.main_horizontal_pic.addEventListener("click", function() 
			{
				self.select_next_photo();
			}, false);

			self.main_vertical_pic.addEventListener("click", function() 
			{
				self.select_next_photo();
			}, false);

			self.next.addEventListener("click", function()
			{
				self.select_next_photo();
			}, false);

			self.next_right.addEventListener("click", function()
			{
				self.select_next_photo();
			}, false);

			self.previous.addEventListener("click", function()
			{
				self.select_previous_photo();
			}, false);

			self.previous_left.addEventListener("click", function()
			{
				self.select_previous_photo();
			}, false);

			self.current_image.addEventListener("load", function()
			{
				self.setup_loaded_photo();
			}, false);

			window.addEventListener("popstate", function(e)
			{
				if(e.state)
				{
					self.current_image.src = e.state.src;
					self.popstate = true;
					var find_current = e.state.src.split("_");
					var file_name = find_current[find_current.length - 1];
					self.current = parseInt(file_name.split(".")[0]);
				}
			}, false);

			window.addEventListener("resize", function()
			{
				self.windowChangedTimerSize = setTimeout(function()
				{
					self.set_correct_button_height();
				}, 500);
			}, false);
		}
	}

	document.addEventListener("DOMContentLoaded", function()
	{
		var gallery = new Gallery();
		gallery.start_presenting(window.location.href);
	});

})();
